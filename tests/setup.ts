import nock from 'nock'
import '@testing-library/jest-dom'
import dotenv from 'dotenv'
import Axios from 'axios'
import { setupIntersectionObserver } from 'tests/utils/mockIntersectionObserver'

dotenv.config({
  path: '.env.test',
})

// eslint-disable-next-line @typescript-eslint/no-empty-function
Object.defineProperty(window, 'scrollTo', { value: () => { } })

beforeAll(() => {
  // ensure axios doesn't try and access localhost
  Axios.defaults.adapter = require('axios/lib/adapters/http')
})

beforeEach(() => {
  // clear all mocks so we don't have to do this every single time
  jest.clearAllMocks()
})

afterEach(function() {
  if (!nock.isDone())
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`Nock has pending mocks ${nock.pendingMocks().join('\n')}
  `)
  nock.cleanAll()
})

setupIntersectionObserver()
