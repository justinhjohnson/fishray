import { render, screen } from '@testing-library/react'
import App from 'pages/_app'

// mock next/router just for this one test
// it's used in the header and we're already testing that functionality
jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathName: '/',
    }
  },
}))

function Component({ foo }: any): JSX.Element {
  return <div>{foo}</div>
}

it('renders the app without errors and passes props', () => {
  render(<App Component={Component} pageProps={{ foo: 'bar' }} />)
  expect(screen.getByText('bar')).toBeInTheDocument()
})
