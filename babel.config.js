const testConfig = {
  presets: ['next/babel', '@babel/preset-typescript'],
}

const prodConfig = {
  presets: ['next/babel'],
}
module.exports = (api) => {
  const isTest = api.env('test')
  return isTest ? testConfig : prodConfig
}
