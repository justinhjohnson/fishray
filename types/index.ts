/* eslint-disable camelcase */
export interface RequestConfig {
  method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELETE'
  data?: unknown
  headers?: Record<string, string>
  params?: Record<string, string | number | boolean>
  preview?: string
}

export interface FetchResponse<T> {
  data: T
  status: number
  statusText: string
}

export type RequestResponse<T extends unknown> = Promise<FetchResponse<T>>

export interface SubmitHandler<T> {
  (a: { values: T; reset(): void }): void
}
