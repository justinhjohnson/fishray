const { isJSDocImplementsTag } = require('typescript')

module.exports = {
  setupFilesAfterEnv: ['<rootDir>/tests/setup.ts'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  collectCoverageFrom: ['pages/**/*{js,jsx,ts,tsx}', 'src/**/*{js,jsx,ts,tsx}'],
  coveragePathIgnorePatterns: [
    '<rootDir>/.next/',
    '<rootDir>/node_modules/',
    '<rootDir>/tests/utils/',
  ],
  moduleDirectories: ['node_modules', './tests', __dirname],
  moduleNameMapper: {
    '\\.(scss|sass|css)$': 'identity-obj-proxy',
  },
  testEnvironment: 'jsdom',
  coverageThreshold: {
    // the global % can be offset by high scores in one directory and low in another
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
    // requires coverage on pages
    './pages': {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
    // requires pages on all src
    // './src': {
    //   branches: 80,
    //   functions: 80,
    //   lines: 80,
    //   statements: 80,
    // },
  },
}
