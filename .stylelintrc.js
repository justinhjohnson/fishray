module.exports = {
  extends: [
    'stylelint-config-standard',
    'stylelint-config-css-modules',
    'stylelint-config-rational-order',
  ],
  plugins: ['stylelint-order'],
  rules: {
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'tailwind',
          'apply',
          'variants',
          'layer',
          'responsive',
          'screen',
        ],
      },
    ],
    'no-missing-end-of-source-newline': null,
    'rule-empty-line-before': null,
    'value-keyword-case': null,
    'function-name-case': null,
    'selector-combinator-space-before': null,
    'selector-combinator-space-after': null,
  },
}
