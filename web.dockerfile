FROM mhart/alpine-node:14

# Set the working directory
WORKDIR /home

# Install dependencies
COPY package.json .
RUN yarn install

# Copy our files
COPY . .

# Send the app init command
CMD ["yarn run dev"]
